// This is not an actual example, it's just a helper
// when using e.g. $QTDIR/examples/script/qscript to
// run examples.
// Evaluate this file after evaluating env.js
// to establish the full Envjs/QtScript environment.
qt.script.importExtension("envjsnatives");
